﻿using System.Web.Mvc;
using System.Web.Routing;

namespace crosscurrentmvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                       name: "Analytics",
                       url: "Solutions/{action}/{id}",
                       defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                   );

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
